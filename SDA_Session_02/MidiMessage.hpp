//
//  MidiMessage.hpp
//  SDA_Session_02
//
//  Created by Giulio Iacomino on 21/10/2021.
//  Copyright © 2021 Giulio Iacomino. All rights reserved.
//

#pragma once
#include <iostream>
#include <cmath>

using namespace std;


class MidiMessage
{
public :
    //constructors
    MidiMessage();
    MidiMessage(int note, int vel, int chan);
    //destructor
    ~MidiMessage();
    
    void setNoteNumber ( int value );
    int getNoteNumber() const;
    float getMidiNoteInHertz() const;
    void setVelocity(int value);
    int getVelocity() const;
    float getFloatVelocity() const;
    void setChannels(int channels);
    // custom clamp template function
    template <typename vtc>
    vtc clamp(vtc input, vtc min, vtc max);
    
    
private :
    int number = 60;
    int velocity = 127;
    int numOfChannels = 2;
    
};
