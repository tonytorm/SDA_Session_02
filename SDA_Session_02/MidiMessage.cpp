//
//  MidiMessage.cpp
//  SDA_Session_02
//
//  Created by Giulio Iacomino on 21/10/2021.
//  Copyright © 2021 Giulio Iacomino. All rights reserved.
//

#include "MidiMessage.hpp"



MidiMessage::MidiMessage()
{
    cout<<"note created"<<endl;
}

MidiMessage::MidiMessage(int note, int vel, int chan)
:   number (note),
    velocity (vel),
    numOfChannels (chan)
{
    cout<<"note create through constructor with arguments: "<<endl;
    cout<<number<<","<<endl;
    cout<<velocity<<","<<endl;
    cout<<numOfChannels<<endl;
}

MidiMessage::~MidiMessage()
{
    cout<<"note destroyed"<<endl;
}

void MidiMessage::setNoteNumber ( int value )
{
    number = clamp(value, 1, 60);
}

int MidiMessage::getNoteNumber() const
{
    return number;
}
    
float MidiMessage::getMidiNoteInHertz() const
{
    return 440.f * std::powf(2.f, (number - 69.f)/ 12.f ) ;
}

void MidiMessage::setVelocity(int value)
{
    velocity = clamp(value, 1, 127);
}

int MidiMessage::getVelocity() const
{
    return velocity;
}

float MidiMessage::getFloatVelocity() const
{
    return velocity/127;
}

void MidiMessage::setChannels(int channels)
{
    numOfChannels = clamp(channels, 1, 2);
}


// custom clamp function
template <typename vtc>
vtc MidiMessage::clamp(vtc input, vtc min, vtc max)
{
    if (input >= max)
        return max;
    else if (input <= min)
        return min;
    else
        return input;
}
